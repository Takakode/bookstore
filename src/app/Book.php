<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title', 'author'
    ];

    /**
     * One day it mays happen that we need to add more specific csv columns.
     * Like if the customer want to see the id or number etc... and are not selectable attributes
     */
    public static function csvColumns($selectedAttributes)
    {
        return $selectedAttributes;
    }

    public function exportCsv()
    {
        $csvArray = [];
        foreach ($this->attributes as $attribute) {
            $csvArray[] = $attribute;
        }

        return $csvArray;
    }

    public function exportXML()
    {
        $xmlBook = '<book>';
        foreach ($this->attributes as $key => $attribute) {
            $xmlBook .= '<' . $key . '>' . $attribute . '</' . $key . '>';
        }

        return $xmlBook . '</book>';
    }
}
