# Book assignment

## Project description

The assignment is to make a crud application for books.
The books are defined by a title and an author. The application should be able to:

- Create a Book.
- Delete a Book.
- Update a Book author.
- To search by title or author.
- Sort by title or author.
- Export to XML or CSV the title and/or author.

There is 2 versions :

- The first version (MVP) should answer to all the needs and have the tests.
- The second version will be a 'training' version. Where I will try to do a better version while trying new technologies, like angular (with Laravel API) and browser test.

## Requirements

- [Docker](https://docs.docker.com/install)
- [Docker Compose](https://docs.docker.com/compose/install)

## Versions change

You can find the first version on the "assignment01" branch and the second version on the "version2" branch
To go to these branch:

- version 1 : `git checkout assignment01`
- version 2 : `git checkout version2`

Recall : `git checkout <branchName>` is a git command to change branch

## Setup

1. Clone the repository.
1. Start the containers by running `docker-compose up -d` in the project root.
1. Install the composer packages by running `docker-compose exec laravel composer install`.
1. Access the Laravel instance on `http://localhost` (If there is a "Permission denied" error, run `docker-compose exec laravel chown -R www-data storage`).

Note that the changes you make to local files will be automatically reflected in the container.

## Persistent database

If you want to make sure that the data in the database persists even if the database container is deleted, add a file named `docker-compose.override.yml` in the project root with the following contents.

```
version: "3.7"

services:
  mysql:
    volumes:
    - mysql:/var/lib/mysql

volumes:
  mysql:
```

Then run the following.

```shell
docker-compose stop \
  && docker-compose rm -f mysql \
  && docker-compose up -d
```

## Tests

To run the test you must connect to your docker container (the one that contains laravel) and then run these commands:

```shell
APP_ENV=testing
php artisan config:clear
vendor/bin/phpunit
```

It wills put the environment to testing mode.
And then we lunch the tests (in the src folder) after cleaning the cache.

After the test re-run to pass to local mode again:

```shell
APP_ENV=local
php artisan config:clear
php artisan migrate
```

Recall : to connect to the a container `docker-compose exec <service> sh`
So in our case :`docker-compose exec laravel sh`
