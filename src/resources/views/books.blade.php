@extends('layouts.app')

@section('content')

    @include('common.errors')
    <!-- New Book Form -->
    <form action="{{ route('books.store') }}" method="POST">
    @csrf
    <div class="row mb-3">
        <label for="book" class="form-label">Book</label>
        <div class="col">
            <label for="book-title" class="form-label">Title</label>
            <input type="text" name="title" id="book-title" class="form-control">
        </div>
        <div class="col">
            <label for="book-author" class="form-label">Author</label>
            <input type="text" name="author" id="book-author" class="form-control">
        </div>
    </div>
    <div class="d-grid justify-content-md-end">
        <button type="submit" class="btn btn-outline-primary text-right">
        <i class="fas fa-book"></i> Add a book
        </button>
    </div>
    </form>
    <div>
        <form action="{{ route('books.export') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-1 mb-3">
                    <div class="form-check form-switch">
                        <input class="form-check-input"  name="title" type="checkbox" id="checkboxTitle" checked>
                        <label class="form-check-label" for="checkTitle">Title</label>
                    </div>
                    <div class="form-check form-switch">
                        <input class="form-check-input" name="author" type="checkbox" id="checkboxAuthor" checked>
                        <label class="form-check-label" for="checkboxAuthor">Author</label>
                    </div>
                </div>
                <div class="col-3 mb-3">
                        <button name="type"
                                value="csv"
                                alt="Download export csv"
                                class="btn btn-outline-primary"
                        >
                            <i class="fas fa-file-csv"></i> Export to CSV
                        </button>
                        <button name="type"
                                value="xml"
                                alt="Download export XML"
                                class="btn btn-outline-primary"
                        >
                            <i class="fas fa-file-excel"></i> Export to XML
                        </button>
                </div>
            </div>
        </form>
    </div>
    <div class="input-group d-grid justify-content-md-end">
        <form action="{{ route('books.search') }}" method="POST">
            @csrf
            <div class="input-group">
                <input  type="text"
                        name="search"
                        id="book-search"
                        class="form-control"
                        placeholder="Search"
                        >
                <button type="submit" class="btn btn-outline-primary text-right">
                    <i class="fas fa-search"></i> Search
                </button>
            </div>
        </form>
        <a href="{{ route('books.index') }}"
            alt="clean the research"
            class="btn btn-outline-primary text-right"
            >
                <i class="fas fa-redo"></i> Clean
        </a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <td scope="col">
                    <a href="{{ route('books.sort', ['orderBy' => 1]) }}"
                        alt="order by title"
                        >
                        Title
                    </a>
                </td>
                <td scope="col">
                    <a href="{{ route('books.sort', ['orderBy' => 2]) }}"
                        alt="order by author"
                        >
                        Author
                    </a>
                </td>
                <td scope="col">
                    Delete
                </td>
            </tr>
        </thead>
        @if (isset($books))
        <tbody>
            @foreach($books as $book)
            <tr>
                <td>{{ $book->title }}</td>
                <td>
                    <form action="{{ route('books.update', $book->id) }}" method="POST" class="form-horizontal">
                        @method('PATCH')
                        @csrf
                        <div class="row">
                            <div class="col-sm-9">
                                <input value="{{ $book->author }}" name="author" id="book-author" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-outline-secondary"><i class="fas fa-edit"></i></button>
                            </div>
                        </div>
                    </form>
                </td>
                <td>
                    <form action="{{ route('books.destroy', $book->id) }}" method="POST" class="form-horizontal">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        @endif
    </table>
</div>

@endsection
