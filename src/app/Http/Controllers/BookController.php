<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the books.
     * And if there is a parameter 1 or 2 sort them by
     * Title and Author respectively
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $books = Book::all();

        if (isset($request->orderBy)) {
            if ($request->orderBy === '1') {
                $books = $books->sortBy('title');
            } else {
                $books = $books->sortBy('author');
            }
        }

        return view('books', compact('books'));
    }

    /**
     * Save the new book after validating the data
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'author' => 'required|max:255',
        ]);

        Book::create($request->all());

        return redirect('/')->with('success', 'Book created successfully');
    }

    /**
     * Delete a book
     *
     * @param Book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return redirect('/')->with('success', 'Book deleted successfully');
    }

    /**
     * Update a book (author only)
     *
     * @param Request $request
     * @param Book $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $request->validate([
            'author' => 'required|max:255',
        ]);

        $book->author = $request->author;
        $book->save();

        return redirect('/')->with('success', 'Book updated successfully');
    }

    /**
     * Update a book (author only)
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $request->validate([
            'search' => 'max:255',
        ]);

        $books = Book::where('author', 'like', '%' . $request->search . '%')->get();
        $books2 = Book::where('title', 'like', '%' . $request->search . '%')->get();
        $books = $books->concat($books2);

        return view('books', compact('books'));
    }

    /**
     * Export the list of books
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $request->validate([
            'type' => 'required|max:3',
        ]);

        $fileName = 'book';
        $selectedAttributes = [];

        if (isset($request->author) && $request->author) {
            $selectedAttributes[] = 'author';
        }

        if (isset($request->title) && $request->title) {
            $selectedAttributes[] = 'title';
        }

        if (empty($selectedAttributes)) {
            $books = Book::all();
            return redirect('books')
                ->withErrors(['error' => 'You didn\'t select any element of Book to export'])
                ->with('books', $books);
        }

        $books = Book::all($selectedAttributes);

        $export = '';
        switch ($request->type) {
            case 'csv':
                $fileName .= '.csv';
                return self::exportCSV($books, $fileName, $selectedAttributes);
                break;
            case 'xml':
                $fileName .= '.xml';
                return self::exportXML($books, $fileName);
                break;
        }
    }

    /**
     * Export a list of books to a csv file name with a filename.
     *
     * @param Array $books
     * @param String $fileName
     * @param Array $selectedAttributes
     *
     * @return \Illuminate\Http\Response
     */
    private static function exportCSV($books, $fileName, $selectedAttributes)
    {
        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
            , 'Content-type' => 'text/csv'
            , 'Content-Disposition' => 'attachment; filename=' . $fileName . '.csv'
            , 'Expires' => '0'
            , 'Pragma' => 'public',
        ];

        $callback = function () use ($books, $fileName, $selectedAttributes) {
            $fileHandler = fopen('php://output', 'w');

            fputcsv($fileHandler, Book::csvColumns($selectedAttributes));
            foreach ($books as $book) {
                fputcsv($fileHandler, $book->exportCsv());
            }

            fclose($fileHandler);
        };

        return response()->streamDownload($callback, $fileName, $headers);
    }

    /**
     * Export a list of books to a xml file name with a filename.
     *
     * @param Array $books
     * @param String $fileName
     *
     * @return \Illuminate\Http\Response
     */
    private static function exportXML($books, $fileName)
    {
        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
            , 'Content-type' => 'text/xml'
            , 'Content-Disposition' => 'attachment; filename=' . $fileName . '.xml'
            , 'Expires' => '0'
            , 'Pragma' => 'public',
        ];

        $callback = function () use ($books, $fileName) {
            $fileHandler = fopen('php://output', 'w');

            fputs($fileHandler, '<?xml version="1.0" encoding="UTF-8" ?>');
            fputs($fileHandler, '<books>');
            foreach ($books as $book) {
                fputs($fileHandler, $book->exportXML());
            }
            fputs($fileHandler, '</books>');
            fclose($fileHandler);
        };

        return response()->streamDownload($callback, $fileName, $headers);
    }
}
