<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/books');
});

Route::resource('books', BookController::class)->except(['create','show']);

Route::get('/books/{orderBy}', 'BookController@index')->name('books.sort');

Route::post('/books/search', 'BookController@search')->name('books.search');

Route::post('/books/export', 'BookController@export')->name('books.export');
